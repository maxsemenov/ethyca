import { createContext, useContext, useMemo, useState } from 'react'

interface IFilterState {
	systemTypes: Record<string, boolean>
	privacyCategories: Record<string, boolean>
}

interface IFilterContext {
	isAnySystemTypeSelected: boolean
	isAnyCategorySelected: boolean
	filters: IFilterState
	toggleSystemType: (typeId: string) => void
	togglePrivacyCategory: (categoryId: string) => void
}

const DEFAULT_FILTER_STATE: IFilterState = {
	systemTypes: {},
	privacyCategories: {}
}

export const FilterContext = createContext<IFilterContext>({
	isAnySystemTypeSelected: false,
	isAnyCategorySelected: false,
	filters: DEFAULT_FILTER_STATE,
	toggleSystemType: () => {},
	togglePrivacyCategory: () => {}
})

export const useFilterContext = () => useContext(FilterContext)

export const useFiltes = (): IFilterContext => {
	const [filtersState, setFiltersState] = useState(DEFAULT_FILTER_STATE)

	return useMemo(
		(): IFilterContext => ({
			filters: filtersState,
			isAnySystemTypeSelected: Object.values(filtersState.systemTypes).some(
				Boolean
			),
			isAnyCategorySelected: Object.values(filtersState.privacyCategories).some(
				Boolean
			),
			togglePrivacyCategory: (categoryId: string) =>
				setFiltersState((_filters) => ({
					..._filters,
					privacyCategories: {
						..._filters.privacyCategories,
						[categoryId]: !_filters.privacyCategories[categoryId]
					}
				})),
			toggleSystemType: (typeId: string) =>
				setFiltersState((_filters) => ({
					..._filters,
					systemTypes: {
						..._filters.systemTypes,
						[typeId]: !_filters.systemTypes[typeId]
					}
				}))
		}),
		[filtersState]
	)
}
