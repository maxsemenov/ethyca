declare module 'Types' {
	export interface ISystem {
		description: string
		fidesKey: string
		name: string
		privacyDeclarations: {
			dataCategories: string[]
			dataSubjects: string[]
			dataUse: string
			name: string
		}[]
		privacyCategories: string[]
		systemDependencies: string[]
		systemType: string
	}

	export interface IGenericDataItem {
		label: string
		id: string
	}
}
