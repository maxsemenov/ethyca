import { useMemo } from 'react'
import { IGenericDataItem, ISystem } from 'Types'
import { System } from '../../views/Systems/System'
import { data } from './data'

interface IUseDataMap {
	systems: ISystem[]
	systemTypes: IGenericDataItem[]
	privacyCategories: IGenericDataItem[]
}

export const useDataMap = (): IUseDataMap => {
	return useMemo(() => {
		const systems: ISystem[] = []
		const types = new Set<string>()
		const categories = new Set<string>()

		for (let j = 0; j < data.length; j++) {
			const privacyCategories = new Set<string>()
			const system = data[j]
			types.add(system.systemType)

			system.privacyDeclarations.forEach((declaration) => {
				for (let i = 0; i < declaration.dataCategories.length; i++) {
					const categoryId =
						declaration.dataCategories[i].split('.').pop() ?? ''

					if (!categoryId) {
						throw new Error(
							`Incorrect privacy category ${declaration.dataCategories[i]}`
						)
					}

					categories.add(categoryId)
					privacyCategories.add(categoryId)
					declaration.dataCategories[i] = categoryId
				}
			})
			systems.push({ ...system, privacyCategories: [...privacyCategories] })
		}

		return {
			systems,
			systemTypes: [...types].map((t) => ({ label: t, id: t })),
			privacyCategories: [...categories].map((c) => ({ label: c, id: c }))
		}
	}, [])
}
