export const data = [
	{
		description:
			'Storefront application to search for products, browse sales and promotions, review product information, etc.',
		fidesKey: 'store_app',
		name: 'Example.com Online Storefront',
		privacyDeclarations: [
			{
				dataCategories: [
					'user.derived.identifiable.device.cookie_id',
					'user.derived.identifiable.device.ip_address',
					'user.derived.identifiable.location'
				],
				dataSubjects: ['customer'],
				dataUse: 'advertising.third_party',
				name: 'Online Advertising'
			},
			{
				dataCategories: ['user.provided.identifiable.contact.email'],
				dataSubjects: ['customer'],
				dataUse: 'advertising.first_party',
				name: 'Email Marketing'
			},
			{
				dataCategories: ['user.derived.identifiable.device.cookie_id'],
				dataSubjects: ['customer'],
				dataUse: 'improve.system',
				name: 'Product Analytics'
			}
		],
		systemDependencies: [
			'app_database',
			'search_database',
			'advertising_integration',
			'email_integration',
			'analytics_integration'
		],
		systemType: 'Application'
	},
	{
		description:
			'Checkout application to collect payment details and submit orders for processing',
		fidesKey: 'checkout_app',
		name: 'Example.com Checkout',
		privacyDeclarations: [
			{
				dataCategories: [
					'user.derived.identifiable.device.cookie_id',
					'user.provided.identifiable.contact.email',
					'user.provided.identifiable.financial'
				],
				dataSubjects: ['customer'],
				dataUse: 'provide.system',
				name: 'eCommerce'
			},
			{
				dataCategories: [
					'user.derived.identifiable.device.cookie_id',
					'user.derived.identifiable.device.ip_address',
					'user.derived.identifiable.location'
				],
				dataSubjects: ['customer'],
				dataUse: 'advertising.third_party',
				name: 'Online Advertising'
			},
			{
				dataCategories: ['user.derived.identifiable.device.cookie_id'],
				dataSubjects: ['customer'],
				dataUse: 'improve.system',
				name: 'Product Analytics'
			}
		],
		systemDependencies: [
			'app_database',
			'orders_service',
			'advertising_integration',
			'analytics_integration',
			'payments_integration'
		],
		systemType: 'Application'
	},
	{
		description:
			'Backend service to process new customer orders, manage inventory, etc.',
		fidesKey: 'orders_service',
		name: 'Orders Management',
		privacyDeclarations: [
			{
				dataCategories: [
					'user.derived.identifiable.device.cookie_id',
					'user.provided.identifiable.contact.email',
					'user.provided.identifiable.financial'
				],
				dataSubjects: ['customer'],
				dataUse: 'provide.system',
				name: 'eCommerce'
			},
			{
				dataCategories: ['user.derived.identifiable.device.cookie_id'],
				dataSubjects: ['customer'],
				dataUse: 'improve.system',
				name: 'Product Analytics'
			}
		],
		systemDependencies: [
			'app_database',
			'analytics_integration',
			'payments_integration'
		],
		systemType: 'Service'
	},
	{
		description:
			'Primary database used to manage account, orders, product, payment data, etc.',
		fidesKey: 'app_database',
		name: 'Example.com Database',
		privacyDeclarations: [
			{
				dataCategories: [
					'user.derived.identifiable.device.cookie_id',
					'user.provided.identifiable.contact.email',
					'user.provided.identifiable.financial'
				],
				dataSubjects: ['customer'],
				dataUse: 'provide.system',
				name: 'eCommerce'
			}
		],
		systemDependencies: [],
		systemType: 'Database'
	},
	{
		description:
			'Search engine used to index product data and provide search results and product recommendations',
		fidesKey: 'search_database',
		name: 'Example.com Search Engine',
		privacyDeclarations: [],
		systemDependencies: [],
		systemType: 'Database'
	},
	{
		description: 'Payments processing integration for eCommerce orders',
		fidesKey: 'payments_integration',
		name: 'Stripe',
		privacyDeclarations: [
			{
				dataCategories: [
					'user.derived.identifiable.device.cookie_id',
					'user.provided.identifiable.contact.email',
					'user.provided.identifiable.financial'
				],
				dataSubjects: ['customer'],
				dataUse: 'provide.system',
				name: 'eCommerce'
			}
		],
		systemDependencies: [],
		systemType: 'Integration'
	},
	{
		description:
			'Email marketing integration to send emails, manage subscriber lists, etc.',
		fidesKey: 'email_integration',
		name: 'Mailchimp',
		privacyDeclarations: [
			{
				dataCategories: ['user.provided.identifiable.contact.email'],
				dataSubjects: ['customer'],
				dataUse: 'advertising.first_party',
				name: 'Email Marketing'
			}
		],
		systemDependencies: [],
		systemType: 'Integration'
	},
	{
		description:
			'Advertising integration to collect audience data and display ads and retargeting campaigns to users',
		fidesKey: 'advertising_integration',
		name: 'Google Ads',
		privacyDeclarations: [
			{
				dataCategories: [
					'user.derived.identifiable.device.cookie_id',
					'user.derived.identifiable.device.ip_address',
					'user.derived.identifiable.location'
				],
				dataSubjects: ['customer'],
				dataUse: 'advertising.third_party',
				name: 'Online Advertising'
			}
		],
		systemDependencies: [],
		systemType: 'Integration'
	},
	{
		description:
			'Analytics integration to track website usage, conversion funnels, etc.',
		fidesKey: 'analytics_integration',
		name: 'Google Analytics',
		privacyDeclarations: [
			{
				dataCategories: ['user.derived.identifiable.device.cookie_id'],
				dataSubjects: ['customer'],
				dataUse: 'improve.system',
				name: 'Product Analytics'
			}
		],
		systemDependencies: [],
		systemType: 'Integration'
	},
	{
		description:
			'Privacy platform to automate the collection and processing of data subject requests for GDPR, CCPA, etc.',
		fidesKey: 'privacy_app',
		name: 'Ethyca',
		privacyDeclarations: [
			{
				dataCategories: [
					'user.derived.identifiable.device.cookie_id',
					'user.provided.identifiable.contact.email',
					'user.derived.identifiable.device.ip_address',
					'user.derived.identifiable.location'
				],
				dataSubjects: ['customer'],
				dataUse: 'provide.system.operations.support',
				name: 'Privacy Requests'
			}
		],
		systemDependencies: [
			'app_database',
			'search_database',
			'payments_integration',
			'email_integration',
			'advertising_integration',
			'analytics_integration'
		],
		systemType: 'Application'
	}
]
