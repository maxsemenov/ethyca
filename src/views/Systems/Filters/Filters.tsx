import styles from './Filters.module.css'

import iconUrl from './filter.svg'
import { useState } from 'react'
import { cn } from '../../../utils/cn'
import { useDataMap } from '../../../modules/dataMap'
import { Tag } from '../../../components/Tag/Tag'
import { useFilterContext } from '../../../modules/filters'

export const Filters: React.FC = () => {
	const [isOpen, setIsOpen] = useState(false)
	const { systemTypes, privacyCategories } = useDataMap()
	const { filters, togglePrivacyCategory, toggleSystemType } =
		useFilterContext()

	const toggle = () => setIsOpen((_isOpen) => !_isOpen)

	return (
		<section className={cn(styles.filters, isOpen && styles.expanded)}>
			<button className={styles.toggle} aria-label="Filters" onClick={toggle}>
				<img src={iconUrl} />
			</button>
			{isOpen && (
				<div className={styles.list}>
					<section className={styles.section}>
						<h3>System Types</h3>
						<ul>
							{systemTypes.map((type) => (
								<li key={type.id}>
									<Tag
										variant="white"
										isSelected={filters.systemTypes[type.id]}
										onClick={() => toggleSystemType(type.id)}
									>
										{type.label}
									</Tag>
								</li>
							))}
						</ul>
					</section>
					<section className={styles.section}>
						<h3>Categories</h3>
						<ul>
							{privacyCategories.map((category) => (
								<li key={category.id}>
									<Tag
										variant="white"
										isSelected={filters.privacyCategories[category.id]}
										onClick={() => togglePrivacyCategory(category.id)}
									>
										{category.label}
									</Tag>
								</li>
							))}
						</ul>
					</section>
				</div>
			)}
		</section>
	)
}
