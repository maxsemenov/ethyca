import { useMemo } from 'react'
import { ISystem } from 'Types'
import { useFilterContext } from '../../modules/filters'
import { System } from './System'

import styles from './SystemGroup.module.css'

interface ISystemGroupProps {
	name: string
	items: ISystem[]
}

export const SystemGroup: React.FC<ISystemGroupProps> = ({ name, items }) => {
	const { filters, isAnyCategorySelected } = useFilterContext()
	const filteredItems = useMemo(() => {
		if (!isAnyCategorySelected) {
			return items
		}
		const selectedCategories = Object.keys(filters.privacyCategories).filter(
			(categoryId) => filters.privacyCategories[categoryId]
		)
		return items.filter((system) =>
			selectedCategories.every((categoryId) =>
				system.privacyCategories.includes(categoryId)
			)
		)
	}, [items, filters.privacyCategories, isAnyCategorySelected])
	return (
		<section className={styles.group}>
			<header className={styles.header}>
				<h2>{name}</h2>
			</header>
			<main className={styles.items}>
				{filteredItems.map((system) => (
					<System key={system.fidesKey} system={system} />
				))}
			</main>
		</section>
	)
}
