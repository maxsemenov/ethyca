import { ISystem } from 'Types'
import { Tag } from '../../components/Tag/Tag'
import { useFilterContext } from '../../modules/filters'

import styles from './System.module.css'

interface ISystemProps {
	system: ISystem
}

export const System: React.FC<ISystemProps> = ({ system }) => {
	const { filters, togglePrivacyCategory } = useFilterContext()
	return (
		<section className={styles.system}>
			<header>
				<h3>{system.name}</h3>
				<p>{system.description}</p>
			</header>
			<main className={styles.declarations}>
				{system.privacyDeclarations.map((declaration) => (
					<div className={styles.declaration} key={declaration.name}>
						<h4>{declaration.name}</h4>
						<ul>
							{declaration.dataCategories.map((category) => {
								return (
									<li key={category}>
										<Tag
											isSelected={filters.privacyCategories[category]}
											onClick={() => togglePrivacyCategory(category)}
										>
											{category}
										</Tag>
									</li>
								)
							})}
						</ul>
					</div>
				))}
			</main>
		</section>
	)
}
