import { groupBy } from 'lodash'
import { createContext, useContext, useMemo, useState } from 'react'
import { useDataMap } from '../../modules/dataMap'
import { FilterContext, useFiltes } from '../../modules/filters'
import { Filters } from './Filters/Filters'
import { SystemGroup } from './SystemGroup'

import styles from './Systems.module.css'

export const Systems: React.FC = () => {
	const { systems } = useDataMap()
	const filtersContext = useFiltes()

	const systemGroups = useMemo(() => groupBy(systems, 'systemType'), [systems])
	const visibleSystemGroups = useMemo(
		() =>
			Object.keys(systemGroups).filter(
				(groupName) =>
					!filtersContext.isAnySystemTypeSelected ||
					filtersContext.filters.systemTypes[groupName]
			),
		[
			systemGroups,
			filtersContext.isAnyCategorySelected,
			filtersContext.filters.systemTypes
		]
	)

	return (
		<FilterContext.Provider value={filtersContext}>
			<div className={styles.container}>
				<Filters />
				<main className={styles.systems}>
					{visibleSystemGroups.map((groupName) => (
						<SystemGroup
							key={groupName}
							name={groupName}
							items={systemGroups[groupName]}
						/>
					))}
				</main>
			</div>
		</FilterContext.Provider>
	)
}
