import { cn } from '../../utils/cn'
import styles from './Tag.module.css'

interface ITagProps extends React.HTMLAttributes<HTMLButtonElement> {
	isSelected?: boolean
	variant?: 'gray' | 'white'
}

export const Tag: React.FC<ITagProps> = ({
	children,
	className,
	isSelected,
	variant = 'gray',
	...rest
}) => {
	return (
		<button
			className={cn(
				styles.tag,
				styles[variant],
				isSelected && styles.selected,
				className
			)}
			{...rest}
		>
			{children}
		</button>
	)
}
