export const cn = (...args: (string | boolean | undefined)[]): string =>
	args.filter(Boolean).join(' ')
